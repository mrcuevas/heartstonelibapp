import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';


import { CardPageModule } from '../card/card.module'
import { CardFavoritePageModule } from '../card/card-favorite/card-favorite.module';
import { CardBackPageModule } from '../card/card-back/card-back.module';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    CardPageModule,
    CardFavoritePageModule,
    CardBackPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
