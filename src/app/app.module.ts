import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { IonicStorageModule } from '@ionic/storage';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { Firebase } from '@ionic-native/firebase/ngx';
import { from } from 'rxjs';
import { FcmServise } from './shared/service/fcm.service';
import { ToastService } from './shared/service/toast.services';



const config = {
  apiKey: "AIzaSyBDa6ObKxjiBP7t2uymZCArqLKZFilUrpI",
  authDomain: "heartstonelib-306f9.firebaseapp.com",
  databaseURL: "https://heartstonelib-306f9.firebaseio.com",
  projectId: "heartstonelib-306f9",
  storageBucket: "heartstonelib-306f9.appspot.com",
  messagingSenderId: "698883171438"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Firebase,
    FcmServise,
    ToastService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
