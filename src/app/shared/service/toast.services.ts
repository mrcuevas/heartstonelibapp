import {  Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ToastService {

  constructor(public toastController: ToastController) { }

  async presentToast(message?: string) {
    const defaultMsg = 'Your settings have been saved.';
    const toast = await this.toastController.create({
      message: message != undefined ? message : defaultMsg,
      duration: 4000
    });
    toast.present();
  }

  async presentErrorToast(message: string) {
    const toast = await this.toastController.create({
      message: `${message}`,
      position: 'middle',
      duration: 6000,
      cssClass: 'toast-error'
    });
    toast.present();
  }

}