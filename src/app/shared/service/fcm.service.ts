import { Injectable } from "@angular/core";
import { Firebase } from "@ionic-native/firebase/ngx";
import { Platform } from "@ionic/angular";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable()
export class FcmServise {
  constructor(private firebase: Firebase, private aFireStore: AngularFirestore, private platform: Platform){}

  async getToken(){
    let token;
    if(this.platform.is('android') ){
      token = await this.firebase.getToken();
    }
    if(this.platform.is('ios')){
      token = await this.firebase.getToken();
      await this.firebase.grantPermission();
    }

    this.saveToken(token);
  }

  private saveToken(token){
    if(!token) return;

    const deviceRef = this.aFireStore.collection('devices');
    const data = {
      token,
      userId: 'testUserID'
    };

    return deviceRef.doc(token).set(data);
  }
  onNotifications(){
    return this.firebase.onNotificationOpen();
  }
}