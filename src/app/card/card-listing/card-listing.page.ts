import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { CardService } from '../shared/card.service';

import { Card } from '../shared/card.model';

import { LoaderService } from '../../shared/service/loader.service';
import { ToastService } from '../../shared/service/toast.services';
import { Storage } from '@ionic/storage';
import { FavoriteCardStorage } from '../shared/card-favorite.store';
import { Subscription } from 'rxjs';
import { InfiniteScroll } from '@ionic/angular';


@Component({
  selector: 'app-card-listing',
  templateUrl: './card-listing.page.html',
  styleUrls: ['./card-listing.page.scss'],
})
export class CardListingPage  {
  cardDeckGroup: string;
  cardDeck: string;
  cards: Card[] = [];
  copyOfCards: Card[] = [];
  isLoading: boolean = true;
  favoriteCards: any= {};
  limit: number = 20;

  favoriteCardSub: Subscription;
  @ViewChild(InfiniteScroll) infiniteScroll: InfiniteScroll;

  constructor(private route: ActivatedRoute, private cardService: CardService, public loaderService: LoaderService, public toastService: ToastService, private storage: Storage, private favoriteCardStorage: FavoriteCardStorage ){
    this.favoriteCardSub = this.favoriteCardStorage.favoriteCars.subscribe( (favoriteCards: any)=> {
      this.favoriteCards = favoriteCards;
    })

  }

  ionViewDidLeave(){
    if (this.favoriteCardSub && !this.favoriteCardSub.closed){
      this.favoriteCardSub.unsubscribe();
    }
  }

  ionViewWillEnter(){
    this.cardDeckGroup = this.route.snapshot.paramMap.get('cardDeckGroup');
    this.cardDeck = this.route.snapshot.paramMap.get('cardDeck');
    if(this.cards && this.cards.length === 0 ) this.getCards();
  }

  private async getCards(){
    await this.loaderService.presentLoading();

    this.cardService.getCardsByDeck(this.cardDeckGroup, this.cardDeck).subscribe((cards: Card[]) => {
      this.cards = cards.map((card: Card) => {
        card.text = this.cardService.replaceCardTextLine(card.text);
        card.favorite = this.isCardFavorite(card.cardId);
        return card;
      });
      this.copyOfCards = Array.from(this.cards);
      this.loaderService.dismissLoadding();
    }, () => {
      this.loaderService.dismissLoadding();
      this.toastService.presentErrorToast("Ups somthing was wrong, try again in a few minutes!");
    })
  }

  private isCardFavorite(cardId: string) : boolean{
    const card = this.favoriteCards[cardId];
    return card ? true : false; 
  }
  refreshCards(event){
    this.getCards();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  hydateCards(cards: Card[]){
    this.cards = cards;
    this.isLoading = false;
  }
  
  handleSearch(){
    this.isLoading= true;
  }

  favoriteCard(card: Card){
    this.favoriteCardStorage.toggleCard(card);
  }

  loadData(event){
    setTimeout(() => {
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.cards.length == this.limit) {
        event.target.disabled = true;
      }else{
        this.limit += 20;
      }
    }, 500);
    

  }
}
