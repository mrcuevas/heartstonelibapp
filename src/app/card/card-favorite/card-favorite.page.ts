import { Component } from '@angular/core';
import { FavoriteCardStorage } from '../shared/card-favorite.store';
import { Subscription } from 'rxjs';
import { Card } from '../shared/card.model';


@Component({
  selector: 'app-card-favorite',
  templateUrl: './card-favorite.page.html',
  styleUrls: ['./card-favorite.page.scss'],
})
export class CardFavoritePage {
  favoriteCardSub: Subscription;
  favoriteCardsList: Card[] = [];
  
  constructor(private favoriteCardStorage: FavoriteCardStorage) {
    this.favoriteCardSub = this.favoriteCardStorage.favoriteCars.subscribe((favoriteCards: any) => {
      this.favoriteCardsList = this.getFavoriteCardList(favoriteCards);
    });

  }

  ionViewDidLeave() {
    if (this.favoriteCardSub && !this.favoriteCardSub.closed) {
      this.favoriteCardSub.unsubscribe();
    }
  }
  // Object.keys(favoriteCards)
  //(5) ["ICCA08_030p", "GVG_030ae", "ICCA08_023e", "GILA_BOSS_20h", "TB_BossRumble_002"]
  private getFavoriteCardList(favoriteCards: any): Card[] {
    if (favoriteCards) {
      return Object.keys(favoriteCards)
        .filter(key => favoriteCards[key])
        .map(key => favoriteCards[key])
    }

    return [];
  }

}
