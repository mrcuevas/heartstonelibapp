import { Component } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { CardService } from '../shared/card.service';
import { Card } from "../shared/card.model";
import { LoaderService } from '../../shared/service/loader.service';
import { ToastService } from '../../shared/service/toast.services';
import { AlertService } from '../../shared/service/alert.service';



@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.page.html',
  styleUrls: ['./card-detail.page.scss'],
})
export class CardDetailPage {
  cardId: string;
  card: Card;

  constructor(private route: ActivatedRoute, private cardService: CardService, public loaderService: LoaderService, public toastService: ToastService, public alertService: AlertService){}

  ionViewWillEnter() {
    this.getCard();
  }

  async getCard(){
    this.cardId = this.route.snapshot.paramMap.get('cardId');
    await this.loaderService.presentLoading()

    this.cardService.getCardById(this.cardId).subscribe((card: Card) => {
      this.card = card[0];
      if (this.card) {
        this.card.text = this.cardService.replaceCardTextLine(this.card.text);
      }
      this.loaderService.dismissLoadding();
    }, () => {
      this.loaderService.dismissLoadding();
      this.alertService.presentAlert("Ups somthing was wrong, try again in a few minutes!");
    })
  }


  updateImage(event: any){
    this.card.img = "assets/images/DefaultCard.png";
  }
}
