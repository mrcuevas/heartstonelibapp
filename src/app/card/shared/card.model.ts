export interface CardDeck {
  name: string;
  types: string[];
}

export interface Card {
  cardId: string;
  cardSet: string;
  img: string;
  imgGold: string;
  name: string;
  favorite: boolean;
  
  attack: number;
  cost: number;
  durability: number;
  health: number;
  type: string;
  rarity: string;
  text: string;
  elite: boolean;
  



  dbfId: string;
  faction: string;
  playerClass: string;
  locale: string;
  mechanics: [any];
}

export interface CardBack {
  cardBackId: number;
  name: string;
  description: string;
  source: string;
  sourceDescription: string;
  howToGet: string,
  enabled: true,
  img: string
  imgAnimated: string;
  sortCategory: number,
  sortOrder: number,
  locale: string;
}