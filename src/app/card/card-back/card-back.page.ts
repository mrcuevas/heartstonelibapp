import { Component, OnInit } from '@angular/core';
import { CardBack } from '../shared/card.model';
import{ CardService } from '../shared/card.service';

@Component({
  selector: 'app-card-back',
  templateUrl: './card-back.page.html',
  styleUrls: ['./card-back.page.scss'],
})
export class CardBackPage {
  cardBackList: CardBack[] = [];

  constructor(private cardService: CardService){}

  ionViewWillEnter(){
    this.getCardBacks();
  }

  private getCardBacks(){
    this.cardService.getAllCardBacks().subscribe( (cardBacks: CardBack[]) =>{
      this.cardBackList = cardBacks.map((cardBack: CardBack) =>{
        cardBack.description = this.cardService.replaceCardTextLine(cardBack.description);
        return cardBack;
      })
    })
  }

}
