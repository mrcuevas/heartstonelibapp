import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
// import { Routes, RouterModule } from '@angular/router';


import { CardDeckPage } from './card-deck/card-deck.page';
import { CardService } from './shared/card.service';
import { CardListComponent } from './components/card-list.component';
import { CardListingPage } from './card-listing/card-listing.page';
import { CardDetailPage } from './card-detail/card-detail.page';
import { LoaderService } from '../shared/service/loader.service';
import { ToastService } from '../shared/service/toast.services';
import { AlertService } from '../shared/service/alert.service';
import { SearchComponent } from '../shared/component/search/search.component';
import { FavoriteCardStorage } from './shared/card-favorite.store';
import { CardFavoritePage } from './card-favorite/card-favorite.page';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    IonicModule,
    // RouterModule.forChild(routes)
  ],
  providers: [
    CardService,
    LoaderService,
    ToastService,
    AlertService,
    FavoriteCardStorage,
    CardFavoritePage
  ],
  declarations: [
    CardDeckPage,
    CardListComponent,
    CardListingPage,
    CardDetailPage,
    SearchComponent
  ]
})
export class CardPageModule {}
